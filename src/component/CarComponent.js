import { Container, Grid, Typography, FormControl, InputLabel, Select, MenuItem, FormLabel, RadioGroup, Radio, FormControlLabel } from "@mui/material"
import { useEffect, useState } from "react";
import { carList } from "../data/carList"

const CarComponent = () => {
    const [brand, setBrand] = useState('');
    const [year, setYear] = useState('');
    const [car, setCar] = useState(carList);

    const handleChangeBrand = (e) => {
        setBrand(e.target.value);
    };

    const handleChangeYear = (e) => {
        setYear(e.target.value);
        console.log(year);
    };

    useEffect(() => {
        const newCarList = carList.filter((value) => {
            return (brand !== "" && year !== "") ? value.name.includes(brand) && value.release_year == year : true;
        })

        setCar(newCarList)
    }, [brand, year])

    return (
        <Container>
            <Grid container>
                <Grid item xs={12} alignSelf="center" textAlign="center" mt={5}>
                    <Typography component="span" lineHeight={3}>Filter by Brand: </Typography>
                    <FormControl sx={{ minWidth: "200px" }}>
                        <InputLabel id="label-age">Brand</InputLabel>
                        <Select
                            id="select-age"
                            value={brand}
                            label="Brand"
                            onChange={handleChangeBrand}
                        >
                            <MenuItem value="Audi">Audi</MenuItem>
                            <MenuItem value="BMW">BMW</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={12} alignSelf="center" textAlign="center" mt={5}>
                    <FormControl>
                        <FormLabel id="radio-buttons-group-label">Filter by Year:</FormLabel>
                        <RadioGroup
                            row
                            defaultValue="2018"
                            name="radio-buttons-group"
                            onChange={handleChangeYear}
                        >
                            <FormControlLabel value={2018} control={<Radio />} label="2018" />
                            <FormControlLabel value={2019} control={<Radio />} label="2019" />
                            <FormControlLabel value={2020} control={<Radio />} label="2020" />
                        </RadioGroup>
                    </FormControl>
                </Grid>
            </Grid>
            <Grid container gap={0}>
                {car.map((value, item) => {
                    return <Grid item xs={4} alignSelf="top" textAlign="center" mt={5} p={1} key={item}>
                        <Typography component="div" border="1px solid" p={3}>
                            <Typography component="p" lineHeight={3}>Name: {value.name}</Typography>
                            <Typography component="p" lineHeight={3}>Year: {value.release_year} </Typography>
                            <Typography
                                component="img"
                                maxWidth="100%"
                                src={value.url}
                                alt={value.name} />
                        </Typography>
                    </Grid>
                })}

            </Grid>
        </Container>
    )
}

export default CarComponent